from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
from .views import GetCustomerByID, CreateCustomer, UpdateCustomer, DeleteCustomer, RetrieveCustomers, GetCustomerByID


urlpatterns = [
    url(_(r'^$'), RetrieveCustomers.as_view(), name='RetrieveCustomers'),
    url(_(r'^create$'), CreateCustomer.as_view(), name='CreateCustomer'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)$'), GetCustomerByID.as_view(), name='GetCustomerByID'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)/update$'), UpdateCustomer.as_view(), name='UpdateCustomer'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)/delete$'), DeleteCustomer.as_view(), name='DeleteCustomer'),
]
