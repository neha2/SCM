from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, DestroyAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response

from apps.Customers.models import Customer
from .serializers import CustomerSerializer


class CreateCustomer(ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response({'reason': serializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)


class UpdateCustomer(RetrieveUpdateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def update_customer(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.data = request.data
        instance.save()

        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class DeleteCustomer(DestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def dalete(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        instance.delete()
        if(serializer.data is None):
            return Response({"Success":"Removed the customer from database"})
        else:
            return Response({"Failure":"Couldn't remove the customer from DB "+serializer.errors})


class RetrieveCustomers(ListAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    class Meta:
        serializer = CustomerSerializer
        fields = "__all__"

class GetCustomerByID(RetrieveAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    def list(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)
