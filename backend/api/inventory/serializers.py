from apps.inventory.models import Item, Grocery, Produce, Vendor
from rest_framework import serializers


class ItemSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Item
        fields = '__all__'


class GrocerySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Grocery
        fields = '__all__'


class ProduceSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Produce
        fields = '__all__'


class VendorSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Vendor
        fields = '__all__'
