from django.conf.urls import url
from .views import index, update_grocery, update_produce, create_grocery, create_produce

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'update_grocery', update_grocery, name='update_grocery'),
    url(r'create_grocery', create_grocery, name='create_grocery'),
    url(r'update_produce', update_produce, name='update_produce'),
    url(r'create_produce', create_produce, name='create_produce'),
]
