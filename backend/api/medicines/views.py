from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, DestroyAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response

from apps.medicines.models import Medicine
from .serializers import MedicineSerializer

import django_filters



class MedicineFilter(django_filters.rest_framework.FilterSet):
    class Meta:
        model = Medicine
        fields = ['manufacturer', 'composition', 'form']

class CreateMedicine(ListCreateAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer


class UpdateMedicine(RetrieveUpdateAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer

    def update_medicine(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.data = request.data
        instance.save()

        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)


class DeleteMedicine(DestroyAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer

    def dalete(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        instance.delete()
        if(serializer.data is None):
            return Response({"Success":"Removed the medicine from database"})
        else:
            return Response({"Failure":"Couldn't remove the medicine from DB "+serializer.errors})


class MedicinesList(ListAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer
    filter_backends =(django_filters.rest_framework.DjangoFilterBackend,)
    filter_class =MedicineFilter

class GetMedicineByID(RetrieveAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer

    def list(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=instance)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)

class RetrieveSimilarMedicines(ListAPIView):
    serializer_class = MedicineSerializer

    def get_queryset(self):
        med_id = self.kwargs['pk']
        med = Medicine.objects.get(id=med_id)
        composition = str(med.composition)
        return Medicine.objects.filter(composition__icontains=med.composition)
