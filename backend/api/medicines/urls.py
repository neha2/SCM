from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
from .views import CreateMedicine, UpdateMedicine, DeleteMedicine, GetMedicineByID, MedicinesList, RetrieveSimilarMedicines

urlpatterns = [
    url(_(r'^create$'), CreateMedicine.as_view(), name='CreateMedicine'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)/update$'), UpdateMedicine.as_view(), name='UpdateMedicine'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)/delete$'), DeleteMedicine.as_view(), name='DeleteMedicine'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)/similar$'), RetrieveSimilarMedicines.as_view(), name='RetrieveSimilarMedicines'),
    url(_(r'^(?P<pk>[0-9a-zA-Z-]+)$'), GetMedicineByID.as_view(), name='GetMedicineByID'),
    url(_(r'^$'), MedicinesList.as_view(), name='MedicinesList'),
]
