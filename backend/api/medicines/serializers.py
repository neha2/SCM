from rest_framework import serializers
from apps.medicines.models import Medicine
from apps.medicines.models import Manufacturer

class ManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manufacturer
        fields = '__all__'
        # fields = ['name']




class MedicineSerializer(serializers.ModelSerializer):
    manufacturer = ManufacturerSerializer()
    class Meta:
        model = Medicine
        fields = '__all__'
