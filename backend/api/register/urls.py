from django.conf.urls import url
from .views import index, product_search

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'product_search/', product_search, name='product_search'),
]
