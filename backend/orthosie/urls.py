from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
from api.inventory.api_views import ItemViewSet, GroceryViewSet, ProduceViewSet, VendorViewSet
from api.register.api_views import ShiftViewSet, TransactionViewSet, LineItemViewSet, TenderViewSet
from api.inventory import urls as inventoryUrls
from api.register import urls as registerUrls
from django.contrib import admin

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^register/', include(registerUrls)),
    url(r'^inventory/', include(inventoryUrls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

router = routers.DefaultRouter()
router.register(r'items', ItemViewSet)
router.register(r'groceries', GroceryViewSet)
router.register(r'produce', ProduceViewSet)
router.register(r'vendors', VendorViewSet)
router.register(r'shifts', ShiftViewSet)
router.register(r'transactions', TransactionViewSet)
router.register(r'line-items', LineItemViewSet)
router.register(r'tenders', TenderViewSet)

urlpatterns += [
    url(r'^', include(router.urls)),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    )
]
