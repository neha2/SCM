from django.db import models

class Vendor(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    def natural_key(self):
        return self.name

    class Meta:
        ordering = ['name']


class Item(models.Model):
    name = models.CharField(max_length=30)
    price = models.DecimalField(max_digits=17, decimal_places=2)
    scalable = models.BooleanField(default=False)
    taxable = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Grocery(Item):
    upc = models.CharField(max_length=30, unique=True)
    vendor = models.ForeignKey(Vendor, default=None, blank=True, null=False)


class Produce(Item):
    plu = models.IntegerField(unique=True)
    variety = models.CharField(max_length=100)
    size = models.CharField(max_length=30, null=True)
    botanical = models.CharField(max_length=100, null=True)


class Upc:

    def __init__(self, upc):
        self.upc = upc

    def verify_check_digit(self):
        if str(self.get_check_digit()) != self.upc[-1]:
            return False
        return True

    def get_check_digit(self):
        check_digit = 0
        odd_pos = True
        for char in self.upc[:-1]:
            if odd_pos:
                check_digit += int(char) * 3
            else:
                check_digit += int(char)
            odd_pos = not odd_pos
        check_digit = (10 - check_digit % 10) % 10
        return check_digit
