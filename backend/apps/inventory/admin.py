from django.contrib import admin
from .models import Vendor, Item, Grocery, Produce

admin.site.register(Vendor)
admin.site.register(Item)
admin.site.register(Grocery)
admin.site.register(Produce)
