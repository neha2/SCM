import uuid
from django.db import models
from accounts.models import User

from django.utils.translation import ugettext_lazy as _


class Customer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User)

    def __str__(self):
        return self.user
