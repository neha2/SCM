import uuid
from django.db import models
from django.utils.translation import ugettext_lazy as _



class Manufacturer(models.Model):
    """
    Model that represents a Manufacturer
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_("Manufacturer's Name"), max_length=200)

    def __str__(self):
        return self.name


class Medicine(models.Model):
    """
    Model that represents a medicine.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(_('Medicine Name'), max_length=200)
    manufacturer = models.ForeignKey(Manufacturer)
    composition = models.CharField(_('Chemical Composition'), max_length=200)
    form = models.CharField(_('Form'),max_length=40)
    mrp = models.FloatField(_('MRP'))

    def __str__(self):

        return self.name

    def get_name(self):
        """
        Return the name of the medicine

        :return: string
        """
        return self.name

    def get_composition(self):
        """
        Return the chemical composition of the medicine.

        :return: string
        """
        return self.composition
