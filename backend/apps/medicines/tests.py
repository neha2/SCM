from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from apps.medicines.models import Medicine

class MedicineTests(APITestCase):
    def create_medicine(self):
        url = reverse('medicines:CreateMedicine')
        data = {
            'name' : 'COGNIDEP 5MG TABLET',
            'composition' : 'Donepezil 5 MG',
            'manufacturer':'ALTEUS BIOGENICS PVT LTD',
            'form':'TABLET',
            'mrp':90.2  # MRP of medicine in float
        }
        response = self.client.post(url,data,format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Medicine.objects.count(), 1)
        self.assertEqual(Medicine.objects.get().name, 'COGNIDEP 5MG TABLET')
