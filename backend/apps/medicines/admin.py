from .models import Medicine, Manufacturer
from django.contrib import admin

admin.site.register(Medicine)
admin.site.register(Manufacturer)
