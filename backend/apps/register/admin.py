from django.contrib import admin
from .models import Shift, Tender, Transaction, LineItem

admin.site.register(Shift)
admin.site.register(Tender)
admin.site.register(Transaction)
admin.site.register(LineItem)
