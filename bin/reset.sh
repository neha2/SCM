#!/bin/bash
# restore a bacakup. arg is a filename that exitsts in backups dir

source bin/env.sh

docker-compose stop

# Remove Migrations
# find . -path "*/migrations/*.py" -not -name "__init__.py" -delete

echo "dropping database..."
dcprod -f docker-compose.db.yml run --rm dbclient dropdb -h db -U $POSTGRES_USER $POSTGRES_DB

echo "creating database..."
dcprod -f docker-compose.db.yml run --rm dbclient createdb -h db -U $POSTGRES_USER -O $POSTGRES_USER $POSTGRES_DB

# Re-applying migrations
# dcdev run --rm  backend ./bin/django.sh makemigrations

echo "reset complete"
