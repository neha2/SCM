import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import SalesPage from "../components/sales";

import { createStructuredSelector } from "reselect";

import { makeSelectInventoryData } from "../selectors/inventorySelector";

import * as inventoryData from "../modules/home";
import * as orderData from "../modules/billing";

const mapStateToProps = createStructuredSelector({
    inventory: makeSelectInventoryData()
});

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...inventoryData,
                ...orderData
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SalesPage);
