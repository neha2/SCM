import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import HomePage from "../components/homePage";

import { createStructuredSelector } from "reselect";

import { makeSelectInventoryData } from "../selectors/inventorySelector";

import * as inventoryData from "../modules/home";

const mapStateToProps = createStructuredSelector({
    inventory: makeSelectInventoryData()
});

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...inventoryData
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
