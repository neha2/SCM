import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import BillingPage from "../components/billing";

import { createStructuredSelector } from "reselect";

import { makeSelectInventoryData } from "../selectors/inventorySelector";
import { makeSelectOrderData } from "../selectors/ordersSelector";

import * as inventoryData from "../modules/home";
import * as orderData from "../modules/billing";

const mapStateToProps = createStructuredSelector({
    inventory: makeSelectInventoryData(),
    orders: makeSelectOrderData()
});

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                ...inventoryData,
                ...orderData
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BillingPage);
