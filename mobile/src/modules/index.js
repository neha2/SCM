import { combineReducers } from "redux-immutable";
import inventoryData from "./home";
import orderDetails from "./billing";
import { fromJS, toJS } from "immutable";

export default combineReducers({
    inventoryData,
    orderDetails
});
