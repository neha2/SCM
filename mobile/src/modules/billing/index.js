import { types } from "../types";

import { fromJS, toJS } from "immutable";

export const getOrder = () => {
    return {
        type: types.GET_ORDER
    };
};
export const addToOrder = order => {
    return {
        type: types.ADD_ORDER,
        order: order
    };
};

const initialState = fromJS({
    orders: [
        {
            ctr: "11.00",
            icon: "pill",
            name: "B-10 Forte",
            price: "12.08",
            stock_quantity: "189",
            type: "pill",
            quantity: "20"
        },
        {
            ctr: "10.00",
            icon: "ointment",
            name: "Volini",
            price: "99.08",
            stock_quantity: "19",
            type: "ointment",
            quantity: "90"
        }
    ]
});
export default (orderDetails = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_ORDER:
            var arr = [action.order];
            return state.set("orders", state.get("orders").concat(fromJS(arr)));

        case types.GET_ORDER:
            return state;

        default:
            return state;
    }
});
