import { types } from "../types";

export const fetchInventoryData = data => {
    return {
        type: types.FETCH_INVENTORY_DATA,
        data: data
    };
};

export const getInventoryData = () => {
    return {
        type: types.GET_INVENTORY_DATA
    };
};

const initialState = {};
export default (inventoryData = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_INVENTORY_DATA:
            return action.data;
        default:
            return state;
    }
});
