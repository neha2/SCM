import { Scene, Router } from "react-native-router-flux";
import React, { Component } from "react";

import Homepage from "./containers/HomePage";
import SalesPage from "./containers/SalesPage";
import Product from "./components/homePage/detailPage";
import BillingPage from "./containers/BillingPage";
import ConfirmOrderPage from "./components/billing/confirmOrder";
import OrderPage from "./components/order";

export default class AppRouter extends React.Component {
    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene
                        style={{ backgroundColor: "white" }}
                        key="BillingPage"
                        hideTabBar="true"
                        component={BillingPage}
                    />
                    <Scene
                        key="Homepage"
                        component={Homepage}
                        hideNavBar
                        initial={true}
                    />
                    <Scene key="Product" hideTabBar component={Product} />

                    <Scene
                        style={{ backgroundColor: "white" }}
                        key="SalesPage"
                        hideTabBar="true"
                        component={SalesPage}
                    />
                    <Scene
                        style={{ backgroundColor: "white" }}
                        key="ConfirmOrderPage"
                        hideTabBar="true"
                        component={ConfirmOrderPage}
                    />
                    <Scene
                        style={{ backgroundColor: "white" }}
                        key="OrderPage"
                        hideTabBar="true"
                        component={OrderPage}
                    />
                </Scene>
            </Router>
        );
    }
}
