import { createSelector } from "reselect";
import { toJS } from "immutable";

const selectOrderDetails = state => state.get("orderDetails");

const makeSelectOrderData = () =>
    createSelector(selectOrderDetails, orderState =>
        orderState.get("orders").toJS()
    );

export { selectOrderDetails, makeSelectOrderData };
