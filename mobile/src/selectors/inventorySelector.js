import { createSelector } from "reselect";
import { toJS } from "immutable";

const selectInventoryDetails = state => state.get("inventoryData");

const makeSelectInventoryData = () =>
    createSelector(selectInventoryDetails, inventoryState => {
        console.log("INVENTORY DATA");
        console.log(inventoryState);
        return inventoryState;
    });

export { selectInventoryDetails, makeSelectInventoryData };
