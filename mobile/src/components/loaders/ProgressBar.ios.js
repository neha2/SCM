
import {React, Component} from 'react';
import { ProgressViewIOS } from 'react-native';

export default class ProgressBar extends Component {


  render() {
    return (
      <ProgressViewIOS
        progress={this.props.progress ? this.props.progress / 100 : 0.5}
      />
    );
  }

}
