import {React, Component} from 'react';
import {ProgressBarAndroid} from 'react-native';


export default class ProgressBar extends Component {
  render() {
    return (
      <ProgressBarAndroid
        styleAttr="Horizontal"
        indeterminate={false}
        progress={this.props.progress ? this.props.progress / 100 : 0.5}
      />
    );
  }

}
