import React, { Component } from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback,
    Keyboard,
    TextInput,
    ListView
} from "react-native";
import AssetMap from "./assets";
import { Actions } from "react-native-router-flux";
import Slider from "react-native-slider";
import TagsRow from "./tag";

import Button from "react-native-button";

const rowHasChanged = (r1, r2) => r1 !== r2;
const tagsds = new ListView.DataSource({ rowHasChanged });

export default class OrderRow extends Component {
    constructor(props) {
        super(props);
        var tags = [
            { id: 1, tag: "Paytm" },
            { id: 2, tag: "Card" },
            { id: 3, tag: "Cash" },
            { id: 4, tag: "TWallet" },
            { id: 5, tag: "FreeCharge" },
            { id: 6, tag: "ApplePay" },
            { id: 7, tag: "XYZ" }
        ];
        this.state = {
            phone: "",
            name: "",
            amount: "",
            sliderValue: 0.5,
            hasFocus: "",
            tags_dataSource: tagsds.cloneWithRows(tags)
        };
    }
    _renderTagsRow(rowData) {
        return <TagsRow data={{ ...rowData }} />;
    }
    setFocus(f) {
        this.setState({ hasFocus: f });
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>
                    Customer
                </Text>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View
                        style={
                            this.state.hasFocus == "name"
                                ? styles.inputBox_focussed
                                : styles.inputBox_unfocussed
                        }
                    >
                        <TextInput
                            style={styles.inputtext}
                            placeholder={"Name"}
                            onChangeText={otp => this.setState({ name })}
                            onFocus={this.setFocus.bind(this, "name")}
                            keyboardType="numeric"
                            value={this.state.name}
                        />
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View
                        style={
                            this.state.hasFocus == "phone"
                                ? styles.inputBox_focussed
                                : styles.inputBox_unfocussed
                        }
                    >
                        <TextInput
                            style={styles.inputtext}
                            placeholder={"Phone Number"}
                            onFocus={this.setFocus.bind(this, "phone")}
                            onChangeText={phone => this.setState({ phone })}
                            keyboardType="numeric"
                            value={this.state.phone}
                        />
                    </View>
                </TouchableWithoutFeedback>
                <Text style={styles.label}>
                    Discount
                </Text>
                <View style={{ marginLeft: 20, marginRight: 20 }}>
                    <Slider
                        value={this.state.sliderValue}
                        minimumTrackTintColor="#870176"
                        thumbTintColor="#870176"
                        onValueChange={value =>
                            this.setState({ sliderValue: value })}
                    />
                </View>
                <Text style={styles.label}>
                    Type of Payment
                </Text>
                <View style={{ marginTop: 20, marginLeft: 20 }}>
                    <ListView
                        horizontal={true}
                        contentContainerStyle={styles.ListViewContainer}
                        renderRow={this._renderTagsRow.bind(this)}
                        dataSource={this.state.tags_dataSource}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

                    <View
                        style={
                            this.state.hasFocus == "amount"
                                ? styles.inputBox_focussed
                                : styles.inputBox_unfocussed
                        }
                    >
                        <TextInput
                            style={styles.inputtext}
                            placeholder={"600"}
                            onChangeText={amount => this.setState({ amount })}
                            onFocus={this.setFocus.bind(this, "amount")}
                            keyboardType="numeric"
                            value={this.state.amount}
                        />
                    </View>
                </TouchableWithoutFeedback>

                <Text style={styles.discount}>
                    Discount Rs.56
                </Text>

                <Text style={styles.amount}>
                    Amount Rs.700
                </Text>
                <View
                    style={{
                        flexDirection: "row",
                        marginTop: 40,
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <Button
                        containerStyle={styles.buttonContainer}
                        style={{
                            fontSize: 14,
                            marginTop: 8,
                            fontWeight: "800",
                            color: "white",
                            alignItems: "center"
                        }}
                        onPress={() => {}}
                    >
                        Save & Print
                    </Button>
                    <Button
                        containerStyle={styles.buttonContainer}
                        style={{
                            fontSize: 14,
                            marginTop: 8,
                            fontWeight: "800",
                            color: "white",
                            alignItems: "center"
                        }}
                        onPress={() => {}}
                    >
                        Save
                    </Button>
                </View>
            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 80
    },
    inputBox_unfocussed: {
        flexDirection: "row",
        marginTop: 10,
        alignItems: "center",
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: "#FFF",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderTopColor: "transparent",
        borderBottomColor: "grey",
        borderWidth: 1
    },
    inputBox_focussed: {
        flexDirection: "row",
        marginTop: 10,
        alignItems: "center",
        marginRight: 20,
        marginLeft: 20,
        backgroundColor: "#FFF",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderTopColor: "transparent",
        borderBottomColor: "#870176",
        borderWidth: 1
    },

    inputtext: {
        height: 40,
        width: Window.width - 80,
        fontSize: 16,
        color: "#9C9C9C"
    },

    thumb: {
        height: 20,
        width: 20,
        marginLeft: 20,
        marginRight: 20
    },
    price: {
        fontSize: 12,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "600"
    },
    buttonContainer: {
        height: 40,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        width: 150,
        overflow: "hidden",
        borderColor: "black",
        borderRadius: 20,
        backgroundColor: "#870176"
    },
    label: {
        fontSize: 18,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 25,
        fontWeight: "400"
    },
    amount: {
        fontSize: 18,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 10,
        fontWeight: "700",
        textAlign: "center"
    },
    discount: {
        fontSize: 10,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 40,
        fontWeight: "200",
        textAlign: "center"
    }
});
