import React, { Component } from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions
} from "react-native";
import AssetMap from "./assets";
import { Actions } from "react-native-router-flux";

export default class OrderRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: 20
        };
    }
    componentWillMount() {
        this.setState({ quantity: this.props.data.quantity });
    }
    render() {
        const {
            quantity,
            icon,
            type,
            stock_quantity,
            price,
            name
        } = this.props.data;
        return (
            <View style={styles.container}>
                <View style={styles.productRow}>
                    <Image style={styles.thumb} source={AssetMap[type]} />
                    <View style={styles.textContainer}>
                        <Text style={styles.name}>
                            {name}
                        </Text>
                        <Text style={styles.price}>
                            Rs. {price}
                        </Text>
                    </View>
                    <View
                        style={{
                            flexDirection: "row"
                        }}
                    >

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    quantity: this.state.quantity + 1
                                });
                            }}
                        >
                            <Image
                                style={styles.thumb}
                                source={require("./images/Add.png")}
                            />
                        </TouchableOpacity>
                        <Text>
                            {quantity}
                        </Text>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    quantity: this.state.quantity - 1
                                });
                            }}
                        >
                            <Image
                                style={styles.thumb}
                                source={require("./images/Remove.png")}
                            />
                        </TouchableOpacity>

                    </View>

                </View>

            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        flex: 1,
        borderColor: "#ECECED",
        borderWidth: 1,
        borderBottomColor: "#ECECED"
    },
    productRow: {
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "center",
        alignItems: "center"
    },

    textContainer: {
        flexDirection: "column",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5,
        marginLeft: 5,
        width: 200,
        justifyContent: "space-between"
    },

    thumb: {
        height: 20,
        width: 20,
        marginLeft: 20,
        marginRight: 20
    },
    price: {
        fontSize: 12,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "600"
    },
    name: {
        fontSize: 15,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "800"
    }
});
