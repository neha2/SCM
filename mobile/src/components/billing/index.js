import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    ScrollView,
    ListView,
    View,
    TouchableOpacity,
    Dimensions
} from "react-native";
import Button from "react-native-button";
import OrderRow from "./order";

import { Actions } from "react-native-router-flux";
const rowHasChanged = (r1, r2) => r1 !== r2;
const orderds = new ListView.DataSource({ rowHasChanged });

export default class Billing extends Component {
    constructor(props) {
        super(props);

        const { orders } = this.props;
        console.log("PROS");
        console.log(this.props);
        var ordersData = orders || [];
        this.state = {
            isOpen: false,
            order_dataSource: orderds.cloneWithRows(ordersData),
            searchText: " "
        };
    }

    _renderOrderRow(rowData) {
        return <OrderRow data={{ ...rowData }} />;
    }

    componentWillMount() {
        this.props.actions.getOrder();
    }
    onCreateButtonClick() {}
    componentWillReceiveProps(nextProps) {
        if (nextProps.orders) {
            this.setState({
                order_dataSource: orderds.cloneWithRows(nextProps.orders)
            });
        }
    }
    render() {
        const { actions } = this.props;
        return (
            <View>
                <View style={styles.logo}>
                    <Image
                        style={styles.imageTick}
                        source={require("./images/logo.png")}
                    />
                </View>
                <View style={styles.profileButton}>
                    <Image
                        style={styles.profile}
                        source={require("./images/profile.png")}
                    />
                </View>
                <View style={styles.searchBar}>
                    <TouchableOpacity style={styles.imageTick}>
                        <Image
                            style={styles.imageTick}
                            source={require("./images/Search.png")}
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={{
                            height: 40,
                            width: Window.width - 80,
                            marginLeft: 25,
                            marginTop: 5
                        }}
                        placeholder="Search by medicine name or chemical composition"
                        onChangeText={text =>
                            this.setState({ searchText: text })}
                        value={this.state.searchText}
                    />
                </View>

                <View style={{ height: 400, marginTop: 20 }}>
                    <ListView
                        renderRow={this._renderOrderRow.bind(this)}
                        dataSource={this.state.order_dataSource}
                        showsVerticalScrollIndicator={false}
                    />
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginTop: 20,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                    />
                </View>
                <Text
                    style={{
                        fontSize: 22,
                        color: "#2F2F2F",
                        marginLeft: 20,
                        marginTop: 5,
                        fontWeight: "800",
                        textAlign: "center"
                    }}
                >
                    Amount Rs. 1000
                </Text>
                <Button
                    containerStyle={styles.createOrderButtonContainer}
                    style={{
                        fontSize: 18,
                        marginTop: 10,
                        fontWeight: "800",
                        color: "white",
                        alignItems: "center"
                    }}
                    onPress={Actions.ConfirmOrderPage}
                >
                    Create Order
                </Button>

            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    buttonContainer: {
        height: 40,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        width: Window.width,
        overflow: "hidden",
        borderColor: "black",
        borderBottomColor: "#F50057",
        borderLeftColor: "#F50057",
        borderRightColor: "#F50057",
        borderTopColor: "#F50057",
        backgroundColor: "white",
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    createOrderButtonContainer: {
        height: 50,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        overflow: "hidden",
        borderColor: "black",
        borderRadius: 25,
        backgroundColor: "#870176",
        marginTop: 30,
        marginLeft: 30,
        marginBottom: 30,
        marginRight: 30
    },

    imageTick: {
        paddingLeft: 10,
        height: 20,
        width: 20
    },
    quantityContainer: {
        flexDirection: "row",
        height: 50,
        width: 100,
        marginBottom: 30,
        marginTop: 10,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFF",
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 6,
        shadowOffset: {
            height: 2,
            width: 2
        }
    },
    profile: {
        paddingLeft: 10,
        height: 25,
        width: 25
    },
    logo: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        left: Window.width / 2 - 5,
        zIndex: 20
    },
    searchBar: {
        marginTop: 65,
        flexDirection: "row",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFF",
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    profileButton: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        right: 15,
        zIndex: 20
    },
    modal: {
        justifyContent: "center",
        alignItems: "center",
        width: 300,
        height: 200
    },
    modal1: {
        height: 300
    },
    spreadButton: {
        position: "absolute",
        height: 20,
        top: 30,
        right: 15,
        zIndex: 20
    },
    ListViewContainer: {
        marginTop: 20,
        marginBottom: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    }
});
