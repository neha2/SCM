import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    ScrollView,
    ListView,
    View,
    TouchableOpacity,
    Dimensions
} from "react-native";
import Modal from "react-native-modalbox";

import { Actions } from "react-native-router-flux";
import Button from "react-native-button";
import ProductRow from "./product";

const rowHasChanged = (r1, r2) => r1 !== r2;
const productds = new ListView.DataSource({ rowHasChanged });

export default class Inventory extends Component {
    constructor(props) {
        super(props);

        const { inventory } = this.props;
        console.log("PROS");
        console.log(this.props);
        var inventoryData = inventory || [];
        this.openModel = this.openModel.bind(this);
        this.state = {
            isOpen: false,
            product_dataSource: productds.cloneWithRows(inventoryData),
            searchText: " ",
            quantityText: "10",
            data: {}
        };
    }
    onClose() {
        console.log("Modal just closed");
    }

    onOpen() {
        console.log("Modal just openned");
    }
    onClosingState(state) {
        console.log("the open/close of the swipeToClose just changed");
    }

    openModel(order_data) {
        this.setState({ isOpen: true });
        this.setState({ data: order_data });
        console.log(this.state.data);
    }
    _renderProductRow(rowData) {
        return <ProductRow data={{ ...rowData }} rowPressed={this.openModel} />;
    }
    onDoneButtonClick() {
        this.setState({ isOpen: false });
        const { data } = this.state;
        const order = {
            name: data.name,
            type: data.type,
            icon: data.icon,
            ctr: data.ctr,
            price: data.price,
            quantity: this.state.quantityText
        };
        this.props.actions.addToOrder(order);
    }
    componentWillMount() {
        this.props.actions.getInventoryData();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.inventory) {
            this.setState({
                product_dataSource: productds.cloneWithRows(nextProps.inventory)
            });
        }
    }
    render() {
        const { actions } = this.props;
        return (
            <View>
                <View style={styles.logo}>
                    <Image
                        style={styles.imageTick}
                        source={require("./images/logo.png")}
                    />
                </View>
                <View style={styles.profileButton}>
                    <Image
                        style={styles.profile}
                        source={require("./images/profile.png")}
                    />
                </View>
                <View style={styles.searchBar}>
                    <TouchableOpacity style={styles.imageTick}>
                        <Image
                            style={styles.imageTick}
                            source={require("./images/Search.png")}
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={{
                            height: 40,
                            width: Window.width - 80,
                            marginLeft: 25,
                            marginTop: 5
                        }}
                        placeholder="Search by medicine name or chemical composition"
                        onChangeText={text =>
                            this.setState({ searchText: text })}
                        value={this.state.searchText}
                    />
                </View>

                <View style={{ height: 600, marginTop: 20 }}>
                    <ListView
                        renderRow={this._renderProductRow.bind(this)}
                        dataSource={this.state.product_dataSource}
                        showsVerticalScrollIndicator={false}
                        renderFooter={() => (
                            <Button
                                containerStyle={styles.buttonContainer}
                                style={{
                                    fontSize: 18,
                                    marginTop: 12,
                                    fontWeight: "900",
                                    color: "black",
                                    alignSelf: "center"
                                }}
                                onPress={() => {}}
                            >
                                Add a New Product {this.state.searchText}
                            </Button>
                        )}
                    />
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginTop: 20,
                            marginLeft: 20,
                            marginRight: 20
                        }}
                    >
                        <TouchableOpacity onPress={Actions.BillingPage}>
                            <Image source={require("./images/sales.png")} />
                        </TouchableOpacity>
                        <Image source={require("./images/order.png")} />
                        <TouchableOpacity onPress={Actions.HomePage}>
                            <Image source={require("./images/stock.png")} />
                        </TouchableOpacity>
                    </View>
                </View>

                <Modal
                    isOpen={this.state.isOpen}
                    onClosed={() => this.setState({ isOpen: false })}
                    style={styles.modal}
                    position={"center"}
                >
                    <View
                        style={{ alignContent: "center", alignItems: "center" }}
                    >

                        <Text
                            style={{
                                fontSize: 18,
                                alignSelf: "center",
                                fontWeight: "bold",
                                marginBottom: 10,

                                color: "black",
                                backgroundColor: "transparent"
                            }}
                        >
                            QUANTITY NEEDED
                        </Text>
                        <View style={styles.quantityContainer}>
                            <TextInput
                                style={{
                                    height: 60,
                                    width: 100,
                                    textAlign: "center"
                                }}
                                onChangeText={text =>
                                    this.setState({ quantityText: text })}
                                value={this.state.quantityText}
                            />
                        </View>
                        <Button
                            containerStyle={styles.modalbuttonContainer}
                            style={{
                                fontSize: 18,
                                marginTop: 8,
                                fontWeight: "600",
                                color: "white",
                                alignItems: "center"
                            }}
                            onPress={() => {
                                this.onDoneButtonClick();
                            }}
                        >
                            Done
                        </Button>

                    </View>
                </Modal>

            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    buttonContainer: {
        height: 40,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        width: Window.width,
        overflow: "hidden",
        borderColor: "black",
        borderBottomColor: "#F50057",
        borderLeftColor: "#F50057",
        borderRightColor: "#F50057",
        borderTopColor: "#F50057",
        backgroundColor: "white",
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    modalbuttonContainer: {
        height: 40,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        width: 200,
        overflow: "hidden",
        borderColor: "black",
        borderRadius: 20,
        backgroundColor: "#870176"
    },
    imageTick: {
        paddingLeft: 10,
        height: 20,
        width: 20
    },
    quantityContainer: {
        flexDirection: "row",
        height: 50,
        width: 100,
        marginBottom: 30,
        marginTop: 10,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFF",
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 6,
        shadowOffset: {
            height: 2,
            width: 2
        }
    },
    profile: {
        paddingLeft: 10,
        height: 25,
        width: 25
    },
    logo: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        left: Window.width / 2 - 5,
        zIndex: 20
    },
    searchBar: {
        marginTop: 65,
        flexDirection: "row",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFF",
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    profileButton: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        right: 15,
        zIndex: 20
    },
    modal: {
        justifyContent: "center",
        alignItems: "center",
        width: 300,
        height: 200
    },
    modal1: {
        height: 300
    },
    spreadButton: {
        position: "absolute",
        height: 20,
        top: 30,
        right: 15,
        zIndex: 20
    },
    ListViewContainer: {
        marginTop: 20,
        marginBottom: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    }
});
