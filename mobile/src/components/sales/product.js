import React, { Component } from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions
} from "react-native";
import AssetMap from "./assets";
import { Actions } from "react-native-router-flux";

export default class ProductRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            styling_color: ""
        };
        this._onRowPress = this._onRowPress.bind(this);
        this._setColor = this._setColor.bind(this);
    }

    _onRowPress() {
        console.log(this.props);
        this.props.rowPressed(this.props.data);
    }

    componentWillMount() {
        this._setColor();
    }

    _setColor() {
        const { stock_quantity } = this.props.data;
        if (stock_quantity >= 150) this.setState({ styling_color: "#337500" });
        else if (stock_quantity >= 100)
            this.setState({ styling_color: "#3F8F01" });
        else if (stock_quantity >= 80)
            this.setState({ styling_color: "#57BF06" });
        else if (stock_quantity >= 30)
            this.setState({ styling_color: "#D1A214" });
        else if (stock_quantity < 30) {
            this.setState({ styling_color: "#AF311D" });
        }
    }

    render() {
        const {
            composition,
            icon,
            type,
            stock_quantity,
            price,
            name
        } = this.props.data;
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this._onRowPress}>
                    <View style={styles.productRow}>
                        <Image style={styles.thumb} source={AssetMap[type]} />
                        <View style={styles.textContainer}>
                            <Text style={styles.name}>
                                {name}
                            </Text>
                            <Text style={styles.composition}>
                                {composition}
                            </Text>
                        </View>
                        <View style={styles.priceContainer}>
                            <Text style={styles.name}>
                                {price}
                            </Text>
                            <Text style={styles.stock}>
                                {stock_quantity}
                            </Text>
                            <View
                                style={[
                                    styles.RatingWrapper,
                                    {
                                        backgroundColor: this.state
                                            .styling_color
                                    }
                                ]}
                            />

                        </View>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        flex: 1,
        borderColor: "#ECECED",
        borderWidth: 1,
        borderBottomColor: "transparent"
    },
    composition: {
        fontSize: 11,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "100",
        width: 200
    },
    icon: {
        height: 10,
        width: 10,
        borderRadius: 10
    },
    RatingWrapper: {
        width: 30,
        height: 5,
        marginLeft: 20
    },
    productRow: {
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    priceContainer: {
        flexDirection: "column",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    textContainer: {
        flexDirection: "column",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "space-between"
    },
    stock: {
        fontSize: 13,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 15,
        marginBottom: 3,
        fontWeight: "100",
        textAlign: "center"
    },
    thumb: {
        height: 30,
        width: 30,
        borderRadius: 15
    },
    name: {
        fontSize: 15,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "800"
    }
});
