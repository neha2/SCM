import React, { Component } from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    StyleSheet,
    Switch,
    Dimensions
} from "react-native";
import AssetMap from "./assets";
import { Actions } from "react-native-router-flux";

export default class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            styling_color: "",
            falseSwitchIsOn: true
        };
        this._setColor = this._setColor.bind(this);
    }

    _onRowPress() {
        Actions.Product({ data: this.props.data });
    }

    componentWillMount() {
        this._setColor();
    }

    _setColor() {
        const { stock_quantity } = this.props.data;
        if (stock_quantity >= 150) this.setState({ styling_color: "#337500" });
        else if (stock_quantity >= 100)
            this.setState({ styling_color: "#3F8F01" });
        else if (stock_quantity >= 80)
            this.setState({ styling_color: "#57BF06" });
        else if (stock_quantity >= 30)
            this.setState({ styling_color: "#D1A214" });
        else if (stock_quantity < 30) {
            this.setState({ styling_color: "#AF311D" });
        }
    }

    render() {
        const {
            composition,
            icon,
            type,
            stock_quantity,
            price,
            name,
            ctr
        } = this.props.data;
        return (
            <View style={styles.container}>

                <Text style={styles.name}>
                    {name}
                </Text>
                <Text style={styles.composition}>
                    {composition}
                </Text>
                <Image style={styles.thumb} source={AssetMap[type]} />

                <View style={styles.priceContainer}>
                    <Text style={styles.label}>
                        CTR ( Cost to Retailer )
                    </Text>
                    <Text style={styles.price}>
                        {ctr}
                    </Text>
                </View>

                <View style={styles.priceContainer}>
                    <Text style={styles.label}>
                        MRP ( Max. Retail Price )
                    </Text>
                    <Text style={styles.price}>
                        {price}
                    </Text>
                </View>

                <View style={styles.priceContainer}>
                    <Text style={styles.label}>
                        Min Stock Reminder
                    </Text>
                    <Text style={styles.price}>
                        50
                    </Text>
                </View>
                <View style={styles.priceContainer}>
                    <Text style={styles.label}>
                        Stock Quantity
                    </Text>
                    <Text style={styles.stock}>
                        {stock_quantity}
                    </Text>
                    <View
                        style={[
                            styles.RatingWrapper,
                            {
                                backgroundColor: this.state.styling_color
                            }
                        ]}
                    />
                </View>

                <View style={styles.priceContainer}>
                    <Text style={styles.label}>
                        Auto-Order
                    </Text>

                    <View style={{ alignItems: "center" }}>
                        <Switch
                            onValueChange={value =>
                                this.setState({ falseSwitchIsOn: value })}
                            style={{
                                marginBottom: 10,
                                marginTop: 10,
                                marginLeft: 10
                            }}
                            value={this.state.falseSwitchIsOn}
                        />
                    </View>

                </View>

            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        flex: 1,
        backgroundColor: "white",
        marginTop: 80,
        alignItems: "center"
    },
    composition: {
        fontSize: 11,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 15,
        fontWeight: "100",
        width: 180,
        textAlign: "center"
    },
    icon: {
        height: 10,
        width: 10,
        borderRadius: 10
    },
    RatingWrapper: {
        width: 30,
        height: 5,
        marginLeft: 20
    },
    productRow: {
        flexDirection: "row",
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    priceContainer: {
        flexDirection: "column",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5,
        marginLeft: 5,
        marginTop: 20,
        justifyContent: "space-between",
        alignItems: "center"
    },
    textContainer: {
        flexDirection: "column",
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 5,
        marginLeft: 5,
        justifyContent: "space-between"
    },
    stock: {
        fontSize: 16,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        marginBottom: 3,
        fontWeight: "800",
        textAlign: "center"
    },
    price: {
        fontSize: 16,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "800"
    },
    thumb: {
        height: 60,
        width: 60,
        marginTop: 30
    },

    label: {
        fontSize: 12,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "400"
    },
    name: {
        fontSize: 26,
        color: "#2F2F2F",
        marginLeft: 20,
        marginTop: 5,
        fontWeight: "800"
    }
});
