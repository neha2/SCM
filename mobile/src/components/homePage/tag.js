import React, { Component } from "react";
import {
    TouchableOpacity,
    View,
    Text,
    Image,
    StyleSheet,
    Dimensions
} from "react-native";

export default class TagsRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        };
    }

    render() {
        const { tag } = this.props.data;
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => {
                    this.setState({ selected: !this.state.selected });
                }}
            >
                <View style={styles.contentWrapper}>
                    {this.state.selected
                        ? <View
                              style={{
                                  width: 100,
                                  height: 40,
                                  borderRadius: 18,
                                  backgroundColor: "#870176"
                              }}
                          >
                              <Text style={styles.title}>
                                  {tag}
                              </Text>
                          </View>
                        : <View
                              style={{
                                  width: 100,
                                  height: 40,
                                  borderRadius: 18,
                                  backgroundColor: "#DDDDDD"
                              }}
                          >
                              <Text style={styles.title}>
                                  {tag}
                              </Text>
                          </View>}

                </View>
            </TouchableOpacity>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        width: Window.width - 310,
        height: 40,
        alignItems: "center"
    },
    title: {
        fontSize: 12,
        color: "white",
        paddingTop: 10,
        fontWeight: "bold",
        backgroundColor: "transparent",
        textAlign: "center"
    },
    contentWrapper: {
        justifyContent: "flex-start",
        flexDirection: "column",
        alignItems: "center"
    }
});
