export default {
    bottle: require("./images/bottle.png"),
    ointment: require("./images/ointment.png"),
    pill: require("./images/pill.png"),
    injection: require("./images/injection.png"),
    spray: require("./images/spray.png"),
    syrup: require("./images/syrup.png")
};
