import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    TextInput,
    Image,
    ScrollView,
    ListView,
    View,
    TouchableOpacity,
    Dimensions
} from "react-native";
import Button from "react-native-button";
import ProductRow from "./product";

import { Actions } from "react-native-router-flux";

const rowHasChanged = (r1, r2) => r1 !== r2;
const products = new ListView.DataSource({ rowHasChanged });
const tags = new ListView.DataSource({ rowHasChanged });

export default class Inventory extends Component {
    constructor(props) {
        super(props);
        var tags = [
            { id: 1, tag: "Ointment" },
            { id: 2, tag: "Pills" },
            { id: 3, tag: "Spray" },
            { id: 4, tag: "Injection" },
            { id: 5, tag: "Syrup" },
            { id: 6, tag: "Gel" },
            { id: 7, tag: "Tablets" }
        ];
        const { inventory } = this.props;
        console.log("PROS");
        console.log(this.props);
        var inventoryData = inventory || [];

        this.state = {
            product_dataSource: products.cloneWithRows(inventoryData)
        };
    }

    _renderProductRow(rowData) {
        return <ProductRow data={{ ...rowData }} />;
    }

    componentWillMount() {
        // this.props.actions.getInventoryData();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.inventory) {
            this.setState({
                product_dataSource: products.cloneWithRows(nextProps.inventory)
            });
        }
    }
    render() {
        const { actions } = this.props;
        return (
            <View>
                <View style={styles.navBar} />
                <View style={styles.logo}>
                    <Image source={require("./images/logo_white.png")} />
                </View>
                <View style={styles.profileButton}>
                    <Image
                        style={styles.profile}
                        source={require("./images/profile_white.png")}
                    />
                </View>
                <View style={styles.searchBar}>
                    <TouchableOpacity style={styles.imageTick}>
                        <Image
                            style={styles.imageTick}
                            source={require("./images/Search.png")}
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={{
                            height: 40,
                            width: Window.width - 80,
                            marginLeft: 25,
                            marginTop: 5
                        }}
                        placeholder="Search by medicine name or chemical composition"
                    />
                </View>
                <View style={{ height: 440, marginTop: 20 }}>
                    <ListView
                        renderRow={this._renderProductRow.bind(this)}
                        dataSource={this.state.product_dataSource}
                        showsVerticalScrollIndicator={false}
                        renderFooter={() => (
                            <Button
                                containerStyle={styles.buttonContainer}
                                style={{
                                    fontSize: 18,
                                    marginTop: 12,
                                    fontWeight: "900",
                                    color: "black",
                                    alignSelf: "center"
                                }}
                                onPress={() => {}}
                            >
                                Add a New Product {this.state.searchText}
                            </Button>
                        )}
                    />
                </View>

                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        marginTop: 20,
                        marginLeft: 20,
                        marginRight: 20
                    }}
                >
                    <TouchableOpacity onPress={Actions.BillingPage}>
                        <Image source={require("./images/sales.png")} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={Actions.SalesPage}>
                        <Image source={require("./images/order.png")} />
                    </TouchableOpacity>
                    <Image source={require("./images/stock.png")} />
                </View>

            </View>
        );
    }
}

let Window = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    navBar: {
        backgroundColor: "#870176",
        borderColor: "#979797",
        borderWidth: 1,
        height: 50,
        marginTop: 20
    },
    buttonContainer: {
        height: 40,
        alignItems: "center",
        marginLeft: 10,
        marginRight: 20,
        width: Window.width,
        overflow: "hidden",
        borderColor: "black",
        borderBottomColor: "#F50057",
        borderLeftColor: "#F50057",
        borderRightColor: "#F50057",
        borderTopColor: "#F50057",
        backgroundColor: "white",
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    imageTick: {
        paddingLeft: 10,
        height: 20,
        width: 20
    },
    profile: {
        paddingLeft: 10,
        height: 25,
        width: 25
    },
    logo: {
        position: "absolute",
        height: 20,
        width: 18,
        top: 30,
        left: Window.width / 2 - 5,
        zIndex: 20
    },
    searchBar: {
        marginTop: 50,
        flexDirection: "row",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#FFF",
        shadowColor: "#000000",
        shadowOpacity: 0.1,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    newOrderButton: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        left: 16,
        zIndex: 20
    },
    profileButton: {
        position: "absolute",
        height: 20,
        width: 20,
        top: 30,
        right: 16,
        zIndex: 20
    },
    spreadButton: {
        position: "absolute",
        height: 20,
        top: 30,
        right: 15,
        zIndex: 20
    },
    ListViewContainer: {
        marginTop: 20,
        marginBottom: 20,
        justifyContent: "center",
        flexDirection: "row",
        flexWrap: "wrap"
    }
});
