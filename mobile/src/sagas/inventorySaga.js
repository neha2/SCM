import { types } from "../modules/types";
import { takeLatest, put } from "redux-saga/effects";
import { fetchInventoryData } from "../modules/home";
import sampleData from "../modules/home/sampleData.json";

export function* getInventoryData() {
    console.log("IN GET DATA");
    try {
        var inventory = [];
        sampleData.inventory.map((value, i) => {
            const product = {
                name: value.name,
                composition: value.chemical_composition,
                icon: value.type,
                type: value.type,
                price: value.price,
                stock_quantity: value.stock_quantity,
                ctr: value.ctr
            };
            inventory.push(product);
        });
        console.log("INVENTORY");
        console.log(inventory);
        yield put(fetchInventoryData(inventory));
    } catch (err) {
        console.log(err);
    }
}

export default function* getInventory() {
    yield takeLatest(types.GET_INVENTORY_DATA, getInventoryData);
}
