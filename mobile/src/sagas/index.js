import getInventory from "./inventorySaga";

export default function* root() {
    yield [getInventory()];
}
