// @flow

import React, { Component } from 'react';
import { Provider } from 'react-redux';

import { View } from 'react-native';
import App from './App';
import configureStore from './configureStore';
import Spinner from './components/loaders/Spinner';

export const storeObj = {};

class Root extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      store: configureStore(() => this.setState({ isLoading: false })),
    };
    storeObj.store = this.state.store;
  }

  render() {
    const content = (!this.state.isLoading ? <Provider store={this.state.store}>
      <App />
    </Provider> : <Spinner />);
    return (
      <View style={{ flex: 1 }} >

        { content }
      </View>
    );
  }
}
export default Root;
