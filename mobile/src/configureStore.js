// @flow

import { createStore, applyMiddleware, compose } from "redux";
import Immutable from "immutable";
import createSagaMiddleware from "redux-saga";
import devTools from "remote-redux-devtools";
import logger from "redux-logger";
import { autoRehydrate, persistStore } from "redux-persist-immutable";
import { AsyncStorage } from "react-native";
import reducer from "./modules/index";
import root from "./sagas";
const sagaMiddleware = createSagaMiddleware();

export default function configureStore(onCompletion): any {
    const enhancer = compose(
        applyMiddleware(sagaMiddleware, logger),
        autoRehydrate(),
        devTools({
            name: "Zmed",
            realtime: true
        })
    );

    const store = createStore(reducer, enhancer);
    persistStore(
        store,
        {
            storage: AsyncStorage,
            blacklist: ["appState"]
        },
        onCompletion
    );

    //  const action = type => store.dispatch({type});//
    sagaMiddleware.run(root).done.catch(error => console.warn(error));
    return store;
}
